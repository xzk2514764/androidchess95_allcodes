package pieces;

import com.example.chess.R;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import board.Board;

public class King extends Piece{
    private boolean hasMoved = false;
    private boolean hasFirstMoved = false;
    private Map<Location, Piece> castleMap = new HashMap<Location,Piece>();

    public King(Location location, boolean isBlack) {
        super(location, isBlack, isBlack ? R.drawable.bk : R.drawable.wk);
    }

    public void calculateAllAvailableMoves(List<Piece> pieces, List<Piece> enemyPieces) {
        super.clearNextAvailableMoves();
        int x = super.getLocation().getX();
        int y = super.getLocation().getY();
        findAvailableMovesByDirection(x, y, 1, 1, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, 1, -1, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, -1, 1, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, -1, -1, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, 0, 1, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, 1, 0, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, -1, 0, pieces, enemyPieces);
        findAvailableMovesByDirection(x, y, 0, -1, pieces, enemyPieces);
        canCastle(x, y, pieces, enemyPieces);
    }

    private void findAvailableMovesByDirection(int x, int y, int xD, int yD, List<Piece> pieces, List<Piece> enemyPieces) {
        x += xD;
        y += yD;
        if(0 <= x && x < Location.BOARDSIZE && 0 <= y && y < Location.BOARDSIZE) {
            Location curLocation = new Location(x,y);
            if(!super.inAttackRange(curLocation, enemyPieces, pieces)) {
                super.addAttackRange(curLocation);
                if (hasPiece(curLocation, pieces)) {
                    return;
                } else {
                    super.addNextAvailableMoves(curLocation);
                }
            }
        }
    }

    public void move(Location location, Board board) {
        super.move(location, board);
        this.hasMoved = super.hasMoved();
        this.hasFirstMoved = super.hasFirstMoved();
        for (Map.Entry<Location,Piece> entry : castleMap.entrySet()) {
            if(location.equals(entry.getKey())) {
                Piece rookPiece = castleMap.get(entry.getKey());
                int x = super.getLocation().getX();
                if(rookPiece.getLocation().getX()> x && !hasFirstMoved) {
                    board.setPrevCastlePiece(rookPiece);
                    rookPiece.move(new Location(x - 1, getLocation().getY()), board);
                } else if(rookPiece.getLocation().getX()< x && !hasFirstMoved){
                    System.out.println("1");
                    board.setPrevCastlePiece(rookPiece);
                    rookPiece.move(new Location(x + 1, getLocation().getY()), board);
                }
                return;
            }
        }
    }

    private void canCastle(int x, int y, List<Piece> pieces, List<Piece> enemyPieces) {
        if(hasMoved) {
            return;
        }
        for(int i = 0; i < pieces.size(); i ++) {
            if(pieces.get(i) instanceof Rook && pieces.get(i).canCastle()) {
                int rookX = pieces.get(i).getLocation().getX();
                Location loc = new Location(x, y);
                if(inAttackRange(loc, enemyPieces, pieces)) {
                    continue;
                }
                if(rookX > x) {
                    loc = new Location(x + 1, y);
                    if(inAttackRange(loc, enemyPieces, pieces) || hasPiece(loc, pieces)) {
                        continue;
                    }

                    loc = new Location(x + 2, y);
                    if(inAttackRange(loc, enemyPieces, pieces) || hasPiece(loc, pieces)) {
                        continue;
                    }

                    if (x + 3 < rookX) {
                        loc = new Location(x + 3, y);
                        if(hasPiece(loc, pieces)) {
                            continue;
                        }
                    }
                    Location nextMove = new Location(x + 2, y);
                    super.addNextAvailableMoves(nextMove);
                    this.castleMap.put(nextMove,  pieces.get(i));
                } else {
                    loc = new Location(x - 1, y);
                    if(inAttackRange(loc, enemyPieces, pieces) || hasPiece(loc, pieces)) {
                        continue;
                    }

                    loc = new Location(x - 2, y);
                    if(inAttackRange(loc, enemyPieces, pieces) || hasPiece(loc, pieces)) {
                        continue;
                    }

                    if (x - 3 > rookX) {
                        loc = new Location(x - 3, y);
                        if(hasPiece(loc, pieces)) {
                            continue;
                        }
                    }
                    Location nextMove = new Location(x - 2, y);
                    super.addNextAvailableMoves(nextMove);
                    this.castleMap.put(nextMove,  pieces.get(i));
                }
            }
        }
    }
}
