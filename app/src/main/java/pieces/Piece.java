package pieces;
import android.location.Address;

import java.util.List;
import java.util.ArrayList;
import board.Board;

public abstract class Piece {

    private Location location;
    private List<Location> nextAvailableMoves = new ArrayList<>();
    private List<Location> attackRange = new ArrayList<>();
    private boolean isBlack;
    public int symbol;
    private int moveCount = 0;
    private boolean enpassant = false;

    public Piece(Location location, boolean isBlack, int symbol) {
        this.location = location;
        this.isBlack = isBlack;
        this.symbol = symbol;
    }

    public boolean isBlack() {
        return isBlack;
    }

    public Location getLocation() {
        return location;
    }

    public void addNextAvailableMoves(Location location) {
        nextAvailableMoves.add(location);
    }

    public void addAttackRange(Location location) {
        attackRange.add(location);
    }

    public List<Location> getNextAvailableMoves() {
        return nextAvailableMoves;
    }

    public boolean isAvailableMove(Location location) {
        for(int i = 0; i < nextAvailableMoves.size(); i ++ ) {
            if(location.equals(nextAvailableMoves.get(i))){
                return true;
            }
        }
        return false;
    }

    public abstract void calculateAllAvailableMoves(List<Piece> pieces, List<Piece> enemyPieces);

    public void clearNextAvailableMoves() {
        nextAvailableMoves = new ArrayList<>();
        attackRange = new ArrayList<>();
    }

    public void move(Location location, Board board) {
        board.clear(this.location);
        this.location = location;
        board.setPiece(this);
    }

    public void addMoveCount() {
        moveCount++;
    }

    public void decreaseMoveCount() {
        moveCount--;
    }

    public boolean hasMoved() {
        return moveCount > 0;
    }

    public boolean hasFirstMoved() {
        return moveCount > 1;
    }

    public void fakeMove(Location location, Board board) {
        board.clear(this.location);
        this.location = location;
        board.setPiece(this);
    }

    public boolean canCastle() {
        return false;
    }

    static public boolean hasPiece(Location location, List<Piece> pieces) {
        for(int i = 0; i < pieces.size(); i++) {
            if(location.equals(pieces.get(i).getLocation())){
                return true;
            }
        }
        return false;
    }

    public int getMoveCount() {
        return moveCount;
    }

    public List<Location> getAttackRange() {
        return attackRange;

    }

    public static boolean inAttackRange(Location location, List<Piece> enemyPieces, List<Piece> pieces) {
        for(int i = 0; i < enemyPieces.size(); i++) {
            for(int j = 0; j < enemyPieces.get(i).getAttackRange().size();j++) {
                if(location.equals(enemyPieces.get(i).getAttackRange().get(j))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean inBlockRange(Location location, List<Piece> Pieces) {
        for(int i = 0; i < Pieces.size(); i++) {
            if(Pieces.get(i) instanceof King) {
                continue;
            }
            for(int j = 0; j < Pieces.get(i).getNextAvailableMoves().size();j++) {
                if(location.equals(Pieces.get(i).getNextAvailableMoves().get(j))) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean getEnpassant() {
        return enpassant;
    }

    public void setEnpassant(boolean setEnpassant) {
        enpassant = setEnpassant;
    }

    static public boolean hasEnpassantPawn(Location location, List<Piece> enemyPieces) {
        for(int i = 0; i < enemyPieces.size(); i++) {
            if(location.equals(enemyPieces.get(i).getLocation())){
                if(enemyPieces.get(i) instanceof Pawn && enemyPieces.get(i).getEnpassant()) {
                    return true;
                }
            }
        }
        return false;
    }
}
