package pieces;

import com.example.chess.R;

import java.util.List;
import board.Board;

public class Pawn extends Piece{
    private boolean hasMoved = false;

    public Pawn(Location location, boolean isBlack) {
        super(location, isBlack, isBlack ? R.drawable.bp : R.drawable.wp);
    }

    public void move(Location location, Board board) {
        Location temp = super.getLocation();
        super.move(location, board);
        this.hasMoved = super.hasMoved();
        if(Math.abs(temp.getY() - super.getLocation().getY()) == 2) {
            super.setEnpassant(true);
        }
    }


    public void calculateAllAvailableMoves(List<Piece> pieces, List<Piece> enemyPieces) {
        super.clearNextAvailableMoves();
        int x =  super.getLocation().getX();
        int y =  super.getLocation().getY();
        if(super.isBlack()) {
            if(findAvailableMovesByDirection(x, y, 0, 1, pieces, enemyPieces) && !hasMoved) {
                findAvailableMovesByDirection(x, y, 0, 2, pieces, enemyPieces);
            }
            findAvailableMovesByDirection(x, y, 1, 1, pieces, enemyPieces);
            findAvailableMovesByDirection(x, y, -1, 1, pieces, enemyPieces);
        } else {
            if(findAvailableMovesByDirection(x, y, 0, -1, pieces, enemyPieces) && !hasMoved) {
                findAvailableMovesByDirection(x, y, 0, -2, pieces, enemyPieces);
            }
            findAvailableMovesByDirection(x, y, 1, -1, pieces, enemyPieces);
            findAvailableMovesByDirection(x, y, -1, -1, pieces, enemyPieces);
        }
        findEnpassant(x, y,  pieces, enemyPieces);

        int dy;
        if(super.isBlack()) {
            dy = 1;
        } else {
            dy = -1;
        }
        if(x + 1 < Location.BOARDSIZE) {
            super.addAttackRange(new Location(x + 1, y + dy));
        }

        if(x - 1 >= 0) {
            super.addAttackRange(new Location(x - 1, y + dy));
        }

    }

    private boolean findAvailableMovesByDirection(int x, int y, int xD, int yD, List<Piece> pieces, List<Piece> enemyPieces) {
        x += xD;
        y += yD;
        if(0 <= x && x < Location.BOARDSIZE && 0 <= y && y < Location.BOARDSIZE) {
            Location curLocation = new Location(x,y);
            if(hasPiece(curLocation, pieces)) {
                return false;
            } else {
                if(hasPiece(curLocation, enemyPieces) && xD != 0) {
                    super.addNextAvailableMoves(curLocation);
                    return true;
                } else if (!hasPiece(curLocation, enemyPieces) && xD == 0) {
                    super.addNextAvailableMoves(curLocation);
                    return true;
                }

            }
        }
        return false;
    }

    private void findEnpassant(int x,int y, List<Piece> pieces, List<Piece> enemyPieces) {
        if(super.isBlack()) {
            if(y == 4) {
                Location curLocationRight = new Location(x+1,y);
                if(hasEnpassantPawn(curLocationRight, enemyPieces)) {
                    super.addNextAvailableMoves(new Location(x+1,5));
                    //System.out.println(new Location(x+1,2));
                }
                Location curLocationLeft = new Location(x-1,y);
                if(hasEnpassantPawn(curLocationLeft, enemyPieces)) {
                    super.addNextAvailableMoves(new Location(x-1,5));
                    //System.out.println(new Location(x-1,5));
                }
            }
        }
        else {
            if(y == 3) {
                Location curLocationRight = new Location(x+1,y);
                if(hasEnpassantPawn(curLocationRight, enemyPieces)) {
                    super.addNextAvailableMoves(new Location(x+1,2));
                    //System.out.println(new Location(x+1,2));
                }
                Location curLocationLeft = new Location(x-1,y);
                if(hasEnpassantPawn(curLocationLeft, enemyPieces)) {
                    super.addNextAvailableMoves(new Location(x-1,2));
                    //System.out.println(new Location(x-1,2));
                }
            }
        }
    }
}
