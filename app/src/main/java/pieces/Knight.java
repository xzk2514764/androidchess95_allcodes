package pieces;

import com.example.chess.R;

import java.util.List;

public class Knight extends Piece{
    public Knight(Location location, boolean isBlack) {
        super(location, isBlack, isBlack ? R.drawable.bn : R.drawable.wn);
    }

    public void calculateAllAvailableMoves(List<Piece> pieces, List<Piece> enemyPieces) {
        super.clearNextAvailableMoves();
        int x = super.getLocation().getX();
        int y = super.getLocation().getY();
        findfirstMovesByDirection(x, y, 1, 2, pieces);
        findfirstMovesByDirection(x, y, 1, -2, pieces);
        findfirstMovesByDirection(x, y, -1, 2, pieces);
        findfirstMovesByDirection(x, y, -1, -2, pieces);
        findfirstMovesByDirection(x, y, 2, 1, pieces);
        findfirstMovesByDirection(x, y, 2, -1, pieces);
        findfirstMovesByDirection(x, y, -2, 1, pieces);
        findfirstMovesByDirection(x, y, -2, -1, pieces);

    }

    private void findfirstMovesByDirection(int x, int y, int xD, int yD, List<Piece> pieces) {
        x += xD;
        y += yD;
        if(0 <= x && x < Location.BOARDSIZE && 0 <= y && y < Location.BOARDSIZE) {
            Location curLocation = new Location(x,y);
            super.addAttackRange(curLocation);
            if(hasPiece(curLocation, pieces)) {
                return;
            } else {
                super.addNextAvailableMoves(curLocation);
            }
        }
    }
}