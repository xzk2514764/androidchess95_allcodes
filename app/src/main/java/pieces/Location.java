package pieces;


public class Location{
    public static int BOARDSIZE = 8;
    int x;
    int y;
    public Location(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean equals(Location location) {
        return this.x==location.x && this.y == location.y;
    }

    public String toString() {
        return "(" + x + " " + y +") ";
    }

    public void setLocation(Location loc) {
        this.x = loc.getX();
        this.y = loc.getY();
    }
}
