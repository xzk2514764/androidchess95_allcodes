package board;

import android.content.Context;

import androidx.appcompat.widget.AppCompatButton;

import com.example.chess.MainActivity;

import java.util.List;


public class GameRecord extends AppCompatButton {
    private List<String> instructions;
    private String time;
    public String name;
    public GameRecord(Context context, String name, List<String> instructions, String time) {
        super(context);
        super.setText(name);
        this.name = name;
        this.instructions = instructions;
        this.time = time;
        MainActivity mainStage = (MainActivity) context;
        this.setOnClickListener(v -> {
            GameRecord gr = (GameRecord) v;
            mainStage.setCurGameRecord(gr);
        });
    }

    public List<String> getInstructions() {
        return instructions;
    }

    public String getName() {
        return name;
    }

    public String getTime() {
        return time;
    }

}
