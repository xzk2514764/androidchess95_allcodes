package board;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.appcompat.widget.AppCompatImageView;

import com.example.chess.MainActivity;

import pieces.Location;
import pieces.Piece;

@SuppressLint("ViewConstructor")
public class SquareView extends AppCompatImageView {
    private final Location loc;
    private Piece piece;
    private final boolean isBlack;
    public SquareView(Context context, int r, int c, Piece p, boolean b, MainActivity mainStage) {
        super(context);
        piece = p;
        isBlack = b;
        loc = new Location(c, r);
        this.setOnClickListener(v -> {
            SquareView sv = (SquareView) v;
            if (sv.piece() != null && mainStage.isRound(sv.piece().isBlack())) {
                System.out.println(r + " " + c);
                mainStage.clearSquare();
                mainStage.setCurSquare(sv);
                return;
            }

            if(mainStage.isInNextAvalableMoves(sv)) {
                mainStage.move(sv);
            }
        });
    }

    public void setPiece(Piece p) {
        piece = p;
    }

    public Location getLocation() {
        return loc;
    }

    public boolean isBlack() {
        return isBlack;
    }

    public Piece piece() {
        return piece;
    }
}
