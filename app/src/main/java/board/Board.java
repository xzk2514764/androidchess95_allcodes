package board;

import com.example.chess.MainActivity;

import pieces.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class Board {
    public static String ALPHABET = "abcdefgh";
    private static String INVALIDINPUT = "Invalid Input, please try again!!!\n";
    private static String INVALIDMOVE = "Invalid Move, please try again!!!\n";

    private List<Piece> blackPieces = new ArrayList<>();
    private List<Piece> whitePieces = new ArrayList<>();
    private Piece prevPiece;
    private Piece prevEliminatedPiece;
    private Piece prevCastlePiece;
    private Location prevCastleLoc;
    private Piece promotionPiece;
    private Piece enpassantPiecce;
    private Location prevLocation;
    private Square[][] LastBoard;


    private class Square {
        private boolean isBlack;
        private Piece piece;
        Square(boolean isBlack){
            this.isBlack = isBlack;
        }

        void setPiece(Piece p) {
            this.piece = p;
        }

        boolean isBlack() {
            return isBlack;
        }
        int getSymbol() {
            if(piece!=null) {
                return piece.symbol;
            }
            else {
                return -1;
            }
        }
        Piece getPiece() { return piece; }

    }
    private Square[][] board = new Square[Location.BOARDSIZE][Location.BOARDSIZE];

    public Board() {
        for(int i = 0; i < Location.BOARDSIZE; i++) {
            for(int j = 0; j < Location.BOARDSIZE; j++)
                this.board[i][j] = new Square((i + j) % 2 == 1);
        }

        for(int i = 0; i < Location.BOARDSIZE; i++) {
            this.board[1][i].setPiece(new Pawn(new Location(i, 1), true));
            blackPieces.add(board[1][i].piece);
            this.board[6][i].setPiece(new Pawn(new Location(i, 6), false));
            whitePieces.add(board[6][i].piece);
        }
        putPieces(true);
        putPieces(false);
        calcAllMoves();
    }

    public boolean getSqaureColor(int r, int c) {
        return board[r][c].isBlack();
    }

    public int getSquarePiece(int r, int c) {
        return board[r][c].getSymbol();
    }

    public Piece getPiece(int r, int c) { return board[r][c].getPiece(); }

    public void clear(Location location) {
        this.board[location.getY()][location.getX()].setPiece(null);;
    }

    public void setPiece(Piece piece) {
        this.board[piece.getLocation().getY()][piece.getLocation().getX()].setPiece(piece);
    }

    private void move1(Piece p, int x2, int y2, boolean realMove) {
        prevLocation = null;
        prevPiece = null;
        promotionPiece = null;
        prevEliminatedPiece = null;
        prevCastlePiece = null;
        prevCastleLoc = null;
        if(board[y2][x2].piece!=null) {
            prevEliminatedPiece = board[y2][x2].piece;
            Piece killedPiece = board[y2][x2].piece;
            if(killedPiece.isBlack()) {
                blackPieces.remove(killedPiece);
            } else {
                whitePieces.remove(killedPiece);
            }
        }
        else if(p instanceof Pawn){
            if(p.isBlack()) {
                if(board[y2-1][x2].piece!=null && board[y2-1][x2].piece.getEnpassant()) {
                    prevEliminatedPiece = board[y2-1][x2].piece;
                    Piece killedPiece = board[y2-1][x2].piece;
                    whitePieces.remove(killedPiece);
                    board[y2-1][x2].piece = null;
                }
            }else {
                if(board[y2+1][x2].piece!=null && board[y2+1][x2].piece.getEnpassant()) {
                    prevEliminatedPiece = board[y2+1][x2].piece;
                    Piece killedPiece = board[y2+1][x2].piece;
                    blackPieces.remove(killedPiece);
                    board[y2+1][x2].piece = null;
                }
            }
        }
        if(realMove) {
            prevLocation = new Location(p.getLocation().getX(), p.getLocation().getY());
            p.addMoveCount();
            p.move(new Location(x2, y2), this);
        } else {
            p.fakeMove(new Location(x2, y2), this);
        }

        calcAllMoves();
    }

    public void undo(Location prevLoc, Location newLoc) {
        newLoc.setLocation(prevPiece.getLocation());
        prevLoc.setLocation(prevLocation);
        prevPiece.decreaseMoveCount();
        prevPiece.move(prevLocation, this);
        if (promotionPiece != null) {
            if (promotionPiece.isBlack()) {
                blackPieces.remove(promotionPiece);
                blackPieces.add(prevPiece);
            }
            else {
                whitePieces.remove(promotionPiece);
                whitePieces.add(prevPiece);
            }
            clear(promotionPiece.getLocation());
        }

        if (prevEliminatedPiece != null) {
            if (prevEliminatedPiece.isBlack()) {
                blackPieces.add(prevEliminatedPiece);
            }
            else {
                whitePieces.add(prevEliminatedPiece);
            }
            setPiece(prevEliminatedPiece);
        }
        if (prevCastlePiece != null) {
            prevCastlePiece.decreaseMoveCount();
            prevCastlePiece.move(prevCastleLoc,this);
        }
        prevLocation = null;
        prevPiece = null;
        promotionPiece = null;
        prevEliminatedPiece = null;
        prevCastlePiece = null;
        prevCastleLoc = null;
        calcAllMoves();
    }

    public void ai(Location start, Location dest, boolean blackRound) {
        List<Piece> pieces;
        if(blackRound) {
            pieces = blackPieces;
        } else {
            pieces = whitePieces;
        }
        List<Piece> pieceOptions = new ArrayList<>();
        for(Piece piece: pieces) {
            if(piece.getNextAvailableMoves().size() > 0) {
                pieceOptions.add(piece);
            }
        }
        Random random = new Random();
        Piece randomPiece = pieceOptions.get(random.nextInt(pieceOptions.size()));
        Location nextMov = randomPiece.getNextAvailableMoves().get(
                random.nextInt(randomPiece.getNextAvailableMoves().size()));
        start.setLocation(randomPiece.getLocation());
        dest.setLocation(nextMov);
    }

    public void setEnpassant(Piece p) {
        enpassantPiecce = p;
    }

    public boolean move(String instruction, boolean turn) {
        String symb = "";
        if(instruction.length() == 7 &&
                instruction.split("\\s+").length == 3 &&
                instruction.split("\\s+")[2].length() == 1) {
            symb = instruction.split("\\s+")[2];
            if(!(symb.equals("Q") || symb.equals("B") || symb.equals("N") || symb.equals("R"))) {
                System.out.println(INVALIDINPUT);
                return false;
            }
            instruction = instruction.substring(0,5);
        }

        if(instruction.length() != 5 || instruction.charAt(2)!=' ') {
            System.out.println(INVALIDINPUT);
            return false;
        }

        int x1 = ALPHABET.indexOf(instruction.charAt(0));
        int x2 = ALPHABET.indexOf(instruction.charAt(3));
        int y1;
        int y2;
        try {
            y1 = Integer.parseInt(String.valueOf(instruction.charAt(1)));
            y2 = Integer.parseInt(String.valueOf(instruction.charAt(4)));
        } catch(Exception e){
            System.out.println(INVALIDINPUT);
            return false;
        }
        y1 = Location.BOARDSIZE - y1;
        y2 = Location.BOARDSIZE - y2;

        if(validIndex(x1) && validIndex(x2) && validIndex(y1) && validIndex(y2)) {
            Piece curPiece = board[y1][x1].piece;
            if(curPiece == null || curPiece.isBlack() == turn) {
                System.out.println(INVALIDMOVE);
                return false;
            }

            if((!symb.equals("")) &&
                    (!(curPiece instanceof Pawn) ||
                            (y2 != 0 && y2 != Location.BOARDSIZE - 1))) {
                System.out.println(INVALIDINPUT);
                return false;
            }

            Location dest = new Location(x2, y2);
            if(curPiece.isAvailableMove(dest)) {
                move1(curPiece, x2, y2, true);
                if(curPiece instanceof Pawn && symb != "") {
                    pawnPromotion(curPiece, symb);
                }
                calcAllMoves();
                prevPiece = curPiece;

                return true;
            }
        }
        System.out.println(INVALIDMOVE);
        return false;
    }

    public boolean isWin(boolean isBlack) {
        List<Piece> pieces;
        if(isBlack) {
            pieces = blackPieces;
        } else {
            pieces = whitePieces;
        }
        for(int i = 0; i < pieces.size(); i ++) {
            if(pieces.get(i) instanceof King) {
                return false;
            }
        }
        return true;
    }

    public boolean check(boolean isBlack) {
        List<Piece> pieces;
        List<Piece> enemyPieces;
        if(isBlack) {
            pieces = blackPieces;
            enemyPieces = whitePieces;
        } else {
            pieces = whitePieces;
            enemyPieces = blackPieces;
        }
        for(int i = 0; i < pieces.size(); i ++) {
            if(pieces.get(i) instanceof King) {
                Location kingLoc = pieces.get(i).getLocation();
                if(Piece.inAttackRange(kingLoc, enemyPieces, pieces)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkmate(boolean isBlack) {
        List<Piece> pieces;
        List<Piece> enemyPieces;
        boolean canBlock = false;
        boolean canTake = false;
        boolean canMoveKing = false;
        if(isBlack) {
            pieces = blackPieces;
            enemyPieces = whitePieces;
        } else {
            pieces = whitePieces;
            enemyPieces = blackPieces;
        }
        for(int i = 0; i < pieces.size(); i ++) {
            if(pieces.get(i) instanceof King) {
                Location kingLoc = pieces.get(i).getLocation();
                for(int x = -1; x < 2; x++) {
                    for(int y = -1; y < 2; y++) {
                        Location loc = new Location(kingLoc.getX() + x,kingLoc.getY() + y);
                        int px = loc.getX();
                        int py = loc.getY();
                        if(!Piece.hasPiece(loc, pieces) && (px <= 7 && py <= 7 && px >= 0 && py >= 0)) {
                            if(!Piece.inAttackRange(loc, enemyPieces, pieces)) {
                                canMoveKing = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        int checker = 0;
        for(int x = 0; x < pieces.size(); x ++) {
            if(pieces.get(x) instanceof King) {
                Location kingLoc = pieces.get(x).getLocation();
                for(int i = 0; i < enemyPieces.size(); i++) {
                    for(int j = 0; j < enemyPieces.get(i).getAttackRange().size();j++) {
                        if(kingLoc.equals(enemyPieces.get(i).getAttackRange().get(j))) {
                            checker++;
                            if(checker == 1) {
                                if(Piece.inAttackRange(enemyPieces.get(i).getLocation(), pieces, enemyPieces)){
                                    canTake = true;
                                }
                            }
                            else {
                                canTake = false;
                            }
                        }
                    }
                }
            }
        }
        if(!(checker > 1)) {
            for(int k = 0; k < pieces.size(); k ++) {
                if(pieces.get(k) instanceof King) {
                    Location kingLoc = pieces.get(k).getLocation();
                    for(int i = 0; i < enemyPieces.size(); i++) {
                        for(int j = 0; j < enemyPieces.get(i).getAttackRange().size();j++) {
                            if(kingLoc.equals(enemyPieces.get(i).getAttackRange().get(j))) {
                                if(enemyPieces.get(i) instanceof Queen || enemyPieces.get(i) instanceof Rook || enemyPieces.get(i) instanceof Bishop) {
                                    int eX = enemyPieces.get(i).getLocation().getX();
                                    int eY = enemyPieces.get(i).getLocation().getY();
                                    int kX = kingLoc.getX();
                                    int kY = kingLoc.getY();
                                    if(eX == kX) {
                                        if(kY > eY) {
                                            for(int a = 1; a < Math.abs(kY - eY); a++) {
                                                Location loc = new Location(eX, eY + a);
                                                if(Piece.inBlockRange(loc, pieces)) {
                                                    canBlock = true;
                                                    break;
                                                }
                                            }
                                        }
                                        else if(kY < eY) {
                                            for(int a = 1; a < Math.abs(kY - eY); a++) {
                                                Location loc = new Location(eX, eY - a);
                                                if(Piece.inBlockRange(loc, pieces)) {
                                                    canBlock = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    else if(eY == kY) {
                                        if(kX > eX) {
                                            for(int a = 1; a < Math.abs(kX - eX); a++) {
                                                Location loc = new Location(eX + a, eY);
                                                if(Piece.inBlockRange(loc, pieces)) {
                                                    canBlock = true;
                                                    break;
                                                }
                                            }
                                        }
                                        else if(kX < eX) {
                                            for(int a = 1; a < Math.abs(kX - eX); a++) {
                                                Location loc = new Location(eX - a, eY);
                                                if(Piece.inBlockRange(loc, pieces)) {
                                                    canBlock = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        if(kX > eX) {
                                            if(kY > eY) {
                                                for(int a = 1; a < Math.abs(kY - eY); a++) {
                                                    Location loc = new Location(eX + a, eY + a);
                                                    if(Piece.inBlockRange(loc, pieces)) {
                                                        canBlock = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            else {
                                                for(int a = 1; a < Math.abs(kY - eY); a++) {
                                                    Location loc = new Location(eX + a, eY - a);
                                                    if(Piece.inBlockRange(loc, pieces)) {
                                                        canBlock = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else if(kX < eX) {
                                            if(kY > eY) {
                                                for(int a = 1; a < Math.abs(kY - eY); a++) {
                                                    Location loc = new Location(eX - a, eY + a);
                                                    if(Piece.inBlockRange(loc, pieces)) {
                                                        canBlock = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            else {
                                                for(int a = 1; a < Math.abs(kY - eY); a++) {
                                                    Location loc = new Location(eX - a, eY - a);
                                                    if(Piece.inBlockRange(loc, pieces)) {
                                                        canBlock = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //System.out.println(canMoveKing +" "+ canTake + " " + canBlock + " " + checker);
        if(canMoveKing || canTake || canBlock) {
            return false;
        }
        else {
            return true;
        }
    }

    private static boolean validIndex(int k) {
        return 0 <= k && k < Location.BOARDSIZE;
    }

    private void putPieces(boolean isBlack) {
        int x;
        if(isBlack) {
            x = 0;
        } else {
            x = Location.BOARDSIZE - 1;
        }
        board[x][0].setPiece(new Rook(new Location(0,x), isBlack));
        board[x][1].setPiece(new Knight(new Location(1, x), isBlack));
        board[x][2].setPiece(new Bishop(new Location(2, x), isBlack));
        board[x][3].setPiece(new Queen(new Location(3, x), isBlack));
        board[x][4].setPiece(new King(new Location(4, x), isBlack));
        board[x][5].setPiece(new Bishop(new Location(5, x), isBlack));
        board[x][6].setPiece(new Knight(new Location(6, x), isBlack));
        board[x][7].setPiece(new Rook(new Location(7, x), isBlack));

        if(isBlack) {
            for(int i = 0; i < Location.BOARDSIZE; i++) {
                blackPieces.add(board[x][i].piece);
            }
        } else {
            for(int i = 0; i < Location.BOARDSIZE; i++) {
                whitePieces.add(board[x][i].piece);
            }
        }
    }

    private void calcAllMoves() {
        for(int i = 0; i < blackPieces.size(); i++) {
            blackPieces.get(i).calculateAllAvailableMoves(blackPieces, whitePieces);
        }
        for(int i = 0; i < whitePieces.size(); i++) {
            whitePieces.get(i).calculateAllAvailableMoves(whitePieces, blackPieces);
        }

        for(int i = 0; i < blackPieces.size(); i++) {
            if(blackPieces.get(i) instanceof King) {
                blackPieces.get(i).calculateAllAvailableMoves(blackPieces, whitePieces);
            }

        }
        for(int i = 0; i < whitePieces.size(); i++) {
            if(whitePieces.get(i) instanceof King) {
                whitePieces.get(i).calculateAllAvailableMoves(whitePieces, blackPieces);
            }
        }

    }

    private void pawnPromotion(Piece p ,String symb) {
        Location loc = p.getLocation();
        int x = loc.getX();
        int y = loc.getY();
        boolean isBlack = true;
        List<Piece> pieces = blackPieces;
        if(y == 0) {
            isBlack = false;
            pieces = whitePieces;
        } else if (y != Location.BOARDSIZE - 1) {
            return;
        }
        pieces.remove(p);
        board[y][x].setPiece(null);
        switch(symb) {
            case "B":
                board[y][x].setPiece(new Bishop(loc, isBlack));
                System.out.println("Pawn promotion to Bishop!\n");
                break;
            case "N":
                board[y][x].setPiece(new Knight(loc, isBlack));
                System.out.println("Pawn promotion to Knight!\n");
                break;
            case "R":
                board[y][x].setPiece(new Rook(loc, isBlack));
                System.out.println("Pawn promotion to Rook!\n");
                break;
            default:
                break;
        }
        if(board[y][x].piece == null) {
            board[y][x].setPiece(new Queen(loc, isBlack));
            System.out.println("Pawn promotion to Queen!\n");
        }
        pieces.add(board[y][x].piece);
        promotionPiece = board[y][x].piece;
    }

    public void ResetEnpassant(boolean turn) {
        if(turn) {
            for(int i = 0; i < whitePieces.size(); i ++) {
                whitePieces.get(i).setEnpassant(false);
            }
        }
        else {
            for(int i = 0; i < blackPieces.size(); i ++) {
                blackPieces.get(i).setEnpassant(false);
            }
        }
    }

    public void setPrevCastlePiece(Piece p){
        prevCastleLoc = p.getLocation();
        prevCastlePiece = p;
    }
}

